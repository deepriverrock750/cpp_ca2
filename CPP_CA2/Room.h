#pragma once
#include"Object.h"
#include"RoomType.h"

class Room : public Object
{
private:
	RoomType m_roomType;
public:
	Room(string name, ObjectType objectType, string description, RoomType roomType);
	~Room();
};

