#pragma once
#include"Character.h"


class Player : public Character
{
private:
	int m_potion;
	bool m_inventory;
public:
	Player(string name, ObjectType objectType, string description, int health, int defense, WeaponType weapon, CharacterType charactertype, int potion, bool inventory);
	~Player();
};

