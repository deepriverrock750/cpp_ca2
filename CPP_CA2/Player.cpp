#include "stdafx.h"
#include "Player.h"


Player::Player(string name, ObjectType objectType, string description, int health, int defense, WeaponType weapon, CharacterType charactertype, int potion, bool inventory):
	Character(name, objectType, description, health, defense, weapon, charactertype)
{
	this->m_potion = potion;
	this->m_inventory = inventory;
}


Player::~Player()
{
}
