#include "stdafx.h"
#include "Character.h"


Character::Character(string name, ObjectType objectType, string description, int health, int defense, WeaponType weapon, CharacterType charactertype): 
	Object(name, objectType, description)
{
	this->m_health = health;
	this->m_defense = defense;
	this->m_weapon = weapon;
	this->m_characterType = charactertype;
}


Character::~Character()
{
}
