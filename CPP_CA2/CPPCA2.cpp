// CPPCA2.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include"ReadFromFile.h"
#include"Character.h"
#include"Room.h"
#include"Object.h"
#include"Player.h"
//#include"GameLogic.h"
#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<list>
#include<vector>
#include<algorithm>
#include "CPPCA2.h"

using namespace std;

vector<string> Maps;
vector<Character> Bosses;
vector<Room> Rooms;
vector<Character> Enemies;


void LoadMaps()
{
	try
	{
		ifstream fileInputStream("Map.txt");
		string map;

		while (!fileInputStream.eof())
		{
			getline(fileInputStream, map, ';');
			istringstream ss(map);
			string temp;
			while (getline(ss, temp, ';'))
			{
				Maps.push_back(temp);
			}
		}
	}
	catch (invalid_argument)
	{
		cout << "File Unsuccessfully Loaded (invalid_argument)1";
		getchar();
	}
}

void LoadBossData()
{
	try
	{
		ifstream fileInputStream("BossData.txt");
		while (!fileInputStream.eof())
		{
			//(string name, ObjectType objectType, string description, int health, int defense, WeaponType weapon, CharacterType charactertype);

			string data, name, description;
			ObjectType objectType;
			WeaponType weaponType;
			CharacterType charactertype;
			int object, weapon, health, character, defense;
			string line;
			int num;
			// Gets the Game Name
			getline(fileInputStream, data, ';');
			name = data;

			
			getline(fileInputStream, data, ';');
			line  = data;
			num = stoi(line);
			objectType = (ObjectType) num;

			getline(fileInputStream, data, ';');
			description = data;
			
			getline(fileInputStream, data, ';');
			health = stoi(data);

			getline(fileInputStream, data, ';');
			defense = stoi(data);

			getline(fileInputStream, data, ';');
			num = stoi(data);
			weaponType = (WeaponType)num;

			getline(fileInputStream, data, ';');
			num = stoi(data);
			charactertype = (CharacterType)num;

			Character newBoss = Character(name, objectType, description, health, defense, weaponType, charactertype);


			//getchar();
			Bosses.push_back(newBoss);
		}
		cout << "File Successfully Loaded" << endl;
		fileInputStream.close();
	}
	catch (invalid_argument)
	{
		cout << "File Unsuccessfully Loaded (invalid_argument)2";
		getchar();
	}
}

void LoadRoomData()
{
	try
	{
		ifstream fileInputStream("RoomDescription.txt");
		while (!fileInputStream.eof())
		{
			//(string name, ObjectType objectType, string description, RoomType roomType);

			string data, name, description;
			ObjectType objectType;
			RoomType roomType;
			int object, room;
			string line;
			int num;
			// Gets the Game Name
			getline(fileInputStream, data, ';');
			name = data;


			getline(fileInputStream, data, ';');
			num = stoi(data);
			objectType = (ObjectType)num;

			getline(fileInputStream, data, ';');
			description = data;

			getline(fileInputStream, data, ';');
			num = stoi(data);
			roomType = (RoomType)num;

			Room newRoom = Room(name, objectType, description, roomType);


			//getchar();
			Rooms.push_back(newRoom);
		}
		cout << "File Successfully Loaded" << endl;
		fileInputStream.close();
	}
	catch (invalid_argument)
	{
		cout << "File Unsuccessfully Loaded (invalid_argument)3";
		getchar();
	}
}

void LoadEnemyData()
{
	try
	{
		ifstream fileInputStream("EnemyData.txt");
		while (!fileInputStream.eof())
		{
			//(string name, ObjectType objectType, string description, int health, int defense, WeaponType weapon, CharacterType charactertype);

			string data, name, description;
			ObjectType objectType;
			WeaponType weaponType;
			CharacterType characterType;
			int object, weapon, health, character, defense;
			int num;
			
			// Gets the Game Name
			getline(fileInputStream, data, ';');
			name = data;


			getline(fileInputStream, data, ';');
			num = stoi(data);
			objectType = (ObjectType)num;

			getline(fileInputStream, data, ';');
			description = data;

			getline(fileInputStream, data, ';');
			health = stoi(data);

			getline(fileInputStream, data, ';');
			defense = stoi(data);

			getline(fileInputStream, data, ';');
			num = stoi(data);
			weaponType = (WeaponType)num;

			getline(fileInputStream, data, ';');
			num = stoi(data);
			characterType = (CharacterType)num;

			Character newEnemy = Character(name, objectType, description, health, defense, weaponType, characterType);


			//getchar();
			Enemies.push_back(newEnemy);
		}
		cout << "File Successfully Loaded" << endl;
		fileInputStream.close();
	}
	catch (invalid_argument)
	{
		cout << "File Unsuccessfully Loaded (invalid_argument)4";
		getchar();
	}
}

void Load()
{
	LoadMaps();
	LoadBossData();
	LoadRoomData();
	LoadEnemyData();
}

int MoveLeft(int mapNum)
{
	if (mapNum = 0)
	{
		mapNum = 1;
	}
	else if (mapNum = 1)
	{
		mapNum = 3;
	}
	else if (mapNum = 2)
	{
		mapNum = 1;
	}
	else if (mapNum = 3)
	{
		mapNum = 6;
	}
	else if (mapNum = 4)
	{
		mapNum = 3;
	}
	else if (mapNum = 5)
	{
		mapNum = 6;
	}
	else if (mapNum = 6)
	{
		mapNum = 10;
	}
	else if (mapNum = 7)
	{
		mapNum = 10;
	}
	else if (mapNum = 8)
	{
		mapNum = 10;
	}
	/*if (mapNum = 9)
	{
	mapNum = 1;
	}*/

	return mapNum;
}

int MoveRight(int mapNum)
{
	if (mapNum = 0)
	{
		mapNum = 1;
	}
	else if (mapNum = 1)
	{
		mapNum = 3;
	}
	else if (mapNum = 2)
	{
		mapNum = 1;
	}
	else if (mapNum = 3)
	{
		mapNum = 6;
	}
	else if (mapNum = 4)
	{
		mapNum = 3;
	}
	else if (mapNum = 5)
	{
		mapNum = 6;
	}
	else if (mapNum = 6)
	{
		mapNum = 10;
	}
	else if (mapNum = 7)
	{
		mapNum = 10;
	}
	else if (mapNum = 8)
	{
		mapNum = 10;
	}
	/*if (mapNum = 9)
	{
	mapNum = 1;
	}*/

	return mapNum;
}

void GameLogic()
{
	bool run = true;
	while (run)
	{
		int mapNum = 0;
		cout << Maps[mapNum];
		int choice;
		cin >> choice;
		switch (choice)
		{
		case 1: // left	
			mapNum = MoveLeft(mapNum);
			cout << Maps[mapNum];
		case 2: // right
			mapNum = MoveRight(mapNum);
			cout << Maps[mapNum];
		}
	}
}

int main()
{
	Load();

	//ReadFromFile();
	//ReadFromFile();
	GameLogic();

	return 0;
}

