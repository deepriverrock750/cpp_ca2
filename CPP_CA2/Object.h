#pragma once
#include<istream>
#include"ObjectType.h"
using namespace std;

class Object
{
private:
	string m_name;
	ObjectType m_objectType;
	string m_description;
public:
	Object(string name, ObjectType objectType, string description);
	virtual void display();
	~Object();
};

