#pragma once
#include"Object.h"
#include"WeaponType.h"
#include"CharacterType.h"

class Character : public Object
{
private:
	int m_health;
	int m_defense;
	WeaponType m_weapon;
	CharacterType m_characterType;
public:
	Character(string name, ObjectType objectType, string description, int health, int defense, WeaponType weapon, CharacterType charactertype);
	~Character();
};

