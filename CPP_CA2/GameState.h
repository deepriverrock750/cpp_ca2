#pragma once
enum GameState
{
	MainMenu,
	PlayerMoving,
	PlayerFighting,
	EnemyFighting,
	Dead,
	Win
};

