// Object 
#include "stdafx.h"
#include "Object.h"
#include<iostream>
#include <string>

using namespace std;

Object::Object(string name, ObjectType objectType, string description)
{
	m_name = name;
	m_objectType = objectType;
	m_description = description;
}

void Object::display()
{
	cout << "Name: " << m_name << "\n " << "Object Type:" << m_objectType << "\n" << "Descrition" << m_description << "\n";
}

Object::~Object()
{
}
